//IMAGE FROM FIRST ONLINE STORE
import projectImg1 from "../assets/images/projectImg/online-store-pc-1.png";
import projectImg2 from "../assets/images/projectImg/online-store-pc-2.png";
import projectImg3 from "../assets/images/projectImg/online-store-pc-3.png";
import projectImg4 from "../assets/images/projectImg/online-store-pc-4.png";
import projectImg5 from "../assets/images/projectImg/online-store-pc-5.png";
import projectImg6 from "../assets/images/projectImg/online-store-pc-6.png";
import projectImg7 from "../assets/images/projectImg/online-store-pc-7.png";
import projectImg8 from "../assets/images/projectImg/online-store-pc-8.png";

//IMAGE FROM SECOND ONLINE STORE
import projectImg9 from "../assets/images/projectImg/online-store-2/online-shop-store-1.png";
import projectImg10 from "../assets/images/projectImg/online-store-2/online-shop-store-2.png";
import projectImg11 from "../assets/images/projectImg/online-store-2/online-shop-store-3.png";
import projectImg12 from "../assets/images/projectImg/online-store-2/online-shop-store-4.png";
import projectImg13 from "../assets/images/projectImg/online-store-2/online-shop-store-5.png";
import projectImg14 from "../assets/images/projectImg/online-store-2/online-shop-store-6.png";

//IMAGE FROM TO-DO-LIST REACT
import to_do_list1 from "../assets/images/projectImg/to-do-list/to-do-list-react-1.png";
import to_do_list2 from "../assets/images/projectImg/to-do-list/to-do-list-react-2.png";

//IMAGE FROM TO-DO-LIST VUE JS
import to_do_list_vue_1 from "../assets/images/projectImg/to-do-list-vue/vue-to-do-1.png";
import to_do_list_vue_2 from "../assets/images/projectImg/to-do-list-vue/vue-to-do-2.png";
import to_do_list_vue_3 from "../assets/images/projectImg/to-do-list-vue/vue-to-do-3.png";
import to_do_list_vue_4 from "../assets/images/projectImg/to-do-list-vue/vue-to-do-4.png";

//IMAGE FROM GAME BLOG
import game_blog_1 from '../assets/images/projectImg/game_blog/game_1.png'
import game_blog_2 from '../assets/images/projectImg/game_blog/game_2.png'
import game_blog_3 from '../assets/images/projectImg/game_blog/game_3.png'
import game_blog_4 from '../assets/images/projectImg/game_blog/game_4.png'
import game_blog_5 from '../assets/images/projectImg/game_blog/game_5.png'

const projectsData = [
  {
    id: 1,
    title: "Online PC Store",
    mainImg: projectImg1,
    imgArr: [
      projectImg1,
      projectImg2,
      projectImg3,
      projectImg4,
      projectImg5,
      projectImg6,
      projectImg7,
      projectImg8,
    ],
    gitLubLink: "https://gitlab.com/Galiyev19/diplome-project",
    gitHubLink: "https://github.com/Galiyev19/online-pc-store.git",
    description:
      "In this project I use React, Redux, React Router, Bootstrap, FireBase, Framer Motion, Node JS for server",
  },
  {
    id: 2,
    title: "Online Shopping Store",
    mainImg: projectImg9,
    imgArr: [
      projectImg9,
      projectImg10,
      projectImg11,
      projectImg12,
      projectImg13,
      projectImg14,
    ],
    gitLubLink: "https://gitlab.com/Galiyev19/online-shopping-store",
    gitHubLink: "https://github.com/Galiyev19/online-shopping-store.git",
    description:
      "In this project I use React, Redux, React Router, Bootstrap, FireBase, API",
    demoLink: "https://shopping-store-react-app.netlify.app/",
  },
  {
    id: 3,
    title: "TO-DO-LIST on REACT",
    mainImg: to_do_list1,
    imgArr: [to_do_list1, to_do_list2],
    gitLubLink: "https://gitlab.com/Galiyev19/to-do-list-react",
    gitHubLink: "https://github.com/Galiyev19/to-do-list-react",
    description: "In this project I use React, Redux",
  },
  {
    id: 4,
    title: "TO-DO-LIST on Vue JS",
    mainImg: to_do_list_vue_1,
    imgArr: [
      to_do_list_vue_1,
      to_do_list_vue_2,
      to_do_list_vue_3,
      to_do_list_vue_4,
    ],
    gitLubLink: "https://gitlab.com/Galiyev19/vue-to-do-list",
    gitHubLink: "https://github.com/Galiyev19/to-do-list-vue",
    description: "In this project I use VUE JS",
  },
  {
    id: 5,
    title: "GAME BLOG",
    mainImg: game_blog_1,
    imgArr: [
        game_blog_1,
        game_blog_2,
        game_blog_3,
        game_blog_4,
        game_blog_5
    ],
    gitLubLink: "https://gitlab.com/Galiyev19/game-store-app.git",
    gitHubLink: "https://github.com/Galiyev19/game_blog.git",
    description: "This project about game. In this project I use React JS, Material UI, Redux",
  },
  // {
  //     id: 3,
  //     title: "TO-DO-LIST",
  //     projects: [
  //         {
  //             id: 1,
  //             nameFrameWork: "React",
  //             images: [ to_do_list1, to_do_list2 ]
  //         },
  //         {
  //             id: 2,
  //             nameFrameWork: "Vue",
  //             images: [projectImg9, projectImg11],
  //         },
  //     ]
  // },
  // {
  //     id: 5,
  //     title: "Online Store",
  //     mainImg: projectImg1,
  //     imgArr: [ projectImg1, projectImg2, projectImg3, projectImg4 ],
  //     gitLubLink: "https://gitlab.com/Galiyev19/diplome-project",
  // },
  // {
  //     id: 6,
  //     title: "Online Store",
  //     mainImg: projectImg1,
  //     imgArr: [ projectImg1, projectImg2, projectImg3, projectImg4 ],
  //     gitLubLink: "https://gitlab.com/Galiyev19/diplome-project",
  // },
];

export default projectsData;
