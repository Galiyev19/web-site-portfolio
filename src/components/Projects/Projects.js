import React from 'react'
import { motion } from 'framer-motion'
import projectsData from "../../helpers/projectListData";

import ProjectList from "../Home/ProjectList/ProjectList";

import "./Projects.css"


const Projects = () => {


    const textAnimation = {
        hidden: {
            x: -100,
            opacity: 0,
        },
        visible:{
            x: 0,
            opacity: 1,
            transition: {delay : 0.4},
        },
    }




    return(
        <main className='projects_container'>
            <div className='main'>
                <motion.div  initial="hidden" whileInView="visible"  className='header_text_projects'>
                    <motion.h1 variants={textAnimation} className="header_title">Projects</motion.h1>
                </motion.div>
               <motion.div>
                    <ProjectList/>
               </motion.div>
            </div>
        </main>
    )
}


export default Projects;