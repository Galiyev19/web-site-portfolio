import React, {useState} from 'react'

import { Container, Navbar, NavLink, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'

import './Header.css'

const Header = () => {

    const [active, setActive] = useState('Home');

  

    return (
            <Navbar bg="dark" expand="md" fixed="top">
                <Container>
                    <Navbar.Brand as={Link} to="/"><span className='logo-text' >Galiyev Alisher</span></Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Nav className='d-flex w-100 justify-content-end' onSelect={(selectedKey) => setActive(selectedKey)}>
                            <NavLink className='menu-link-text' as={Link} to="/" eventKey="Home">Home</NavLink>
                            <NavLink className='menu-link-text' as={Link} to="/projects" eventKey="Projects">Projects</NavLink>
                            <NavLink className='menu-link-text' as={Link} to="/contacts" eventKey="Contacts">Contact Me</NavLink>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        
    )
}

export default Header;