import React, { useState } from 'react'
import {useParams , Link} from 'react-router-dom'

import projectsData from "../../helpers/projectListData";

import { FcPrevious, FcNext } from 'react-icons/fc'
import { FiGitlab } from 'react-icons/fi'
import {VscGithub} from 'react-icons/vsc'
import { motion } from "framer-motion";

import "./ProjectDetail.css"



const ProjectDetail = () => {

    const { id } = useParams()
    // const { title, imgArr, description } = projectsData[id]
    const project = projectsData.find(item => item.id === parseInt(id))

     // console.log(project)

    const [index,setIndex] = useState(0)
    const [direction, setDirection] = useState(0)

    const prevStep = () => {
        setDirection(-1)
        if(index === 0) {
            setIndex(project.imgArr.length - 1)
            return
        }

        setIndex(index - 1)
    }

    const nextStep = () => {
        setDirection(1)
        if(index === project.imgArr.length - 1){
            setIndex(0)
            return
        }

        setIndex(index + 1)
    }



    const prevStepTest = ( id ) => {
        const idx = project.projects.findIndex(project => project.id === id)

        // console.log(project.projects[idx])
        setDirection(-1)

        if(index === 0) {
            setIndex(project.projects[idx]?.images.length - 1)
            return
        }

        setIndex(index - 1)


    }



    const nextStepTest = (id) => {
        const idx = project.projects.findIndex(project => project.id === id)
        // console.log(idx)
        setDirection(1)
        if(index === project.projects[idx]?.images.length - 1){
            setIndex(0)
            return
        }

        setIndex(index + 1)



    }




    const variants = {
        initial: direction => ({
            // y: "100%",
            opacity: 0
        }),
        animate: {
            // y: 0,
            opacity: 1,
            transition: { type: "spring", stiffness: 50 },
        },
        exit: direction => ({
            x: -200,
            opacity: 0
        })
    }

    return (
        <motion.main className="projectDetail_container"
            initial={{y:"100%"}}
            animate={{y: 0}}
            transition={{duration: 1, ease: "easeOut"}}
            exit={{opacity: 1}}
        >
            <div className="main">
                <div className="project_title_block">
                    <h1 className='header_title'>{project.title}</h1>
                </div>
                {
                   

                    <div className="slideShow">
                        {
                            project.imgArr.map(item =>
                                <div key={item} className="d-flex w-100 h-100">
                                    <motion.img
                                        variants={variants}
                                        animate='animate'
                                        initial='initial'
                                        transition={{duration: .75, delay: .3}}
                                        exit='exit'
                                        custom={direction}
                                        key={project.imgArr[index]}
                                        src={project.imgArr[index]}
                                        alt="slides"
                                        className="slides"/>
                                    <button className="slider_btn prevBtn" onClick={() => prevStep()}>
                                        <FcPrevious className="fs-2"/>
                                    </button>
                                    <button className="slider_btn nextBtn" onClick={() => nextStep()}>
                                        <FcNext className="fs-2"/>
                                    </button>
                                </div>)
                        }
                    </div>

                }

                <div className="description">
                    <p className="header_text w-50">{project.description}</p>
                </div>
                <div className="git_btn">
                    <Link className="btn_repo m-3" to={project.gitLubLink}>
                        <FiGitlab className="me-2"/>
                        GitLab repo
                    </Link>
                    <Link className="btn_repo" to={project.gitHubLink}>
                        <VscGithub className="me-2"/>
                        GitHub repo
                    </Link>
                    {project.demoLink ? 
                    <Link className='btn_repo m-3' to={project.demoLink} target='_blank'>Demo</Link> 
                    : null}
                </div>
            </div>
        </motion.main>
    )


}

export default ProjectDetail;