import React from 'react'

import {BiPhoneCall} from 'react-icons/bi'
import {TfiEmail} from 'react-icons/tfi'
import {CiLocationOn} from 'react-icons/ci'

import ContactForm from "./ContactForm";

import "./ContactMe.css"

const ContactMe = () => {

    return(
        <div className="contact_me_container">
            <div className="main_contact_me">
                <div className="d-flex flex-column align-items-center justify-content-center w-100 text-center">
                    <h1 className="header_title">Contact me</h1>
                    <span className="header_text">Get in touch</span>
                </div>
                <div className="contact_form">
                    <div className="contact_form_sides">
                        <div className="d-flex mb-5">
                            <BiPhoneCall className="fs-2"/>
                            <div className="d-flex align-items-start flex-column mx-3">
                                <span className="header_text">Call Me</span>
                                <a href="tel:+77087777412" className="text-secondary">+7 (708) 777-74-12</a>
                            </div>
                        </div>
                        <div className="d-flex mb-5">
                            <TfiEmail className="fs-2"/>
                            <div className="d-flex align-items-start flex-column mx-3">
                                <span className="header_text">Email</span>
                                <span className="text-secondary">galiyevalisher7@gmail.com</span>
                            </div>
                        </div>
                        <div className="d-flex mb-5">
                            <CiLocationOn className="fs-2"/>
                            <div className="d-flex align-items-start flex-column mx-3">
                                <span className="header_text">Location</span>
                                <span className="text-secondary">Astana,Kazakhstan</span>
                            </div>
                        </div>
                    </div>
                    <div className="contact_form_sides">
                        <ContactForm/>
                    </div>
                </div>
            </div>
        </div>
    )

}


export default ContactMe;