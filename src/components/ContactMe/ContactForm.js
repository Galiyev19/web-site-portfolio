import React from "react";
import {useForm} from "react-hook-form";
import emailjs from '@emailjs/browser';

import Form from 'react-bootstrap/Form'

import {AiOutlineSend} from 'react-icons/ai'

import "./ContactForm.css"

const ContactForm = () => {

    const service_id = 'service_fi63cse'
    const template_id = 'template_qc0idst'
    const key = 'aMqaQt9FsAWhmu2v3'


    const {
        register,
        reset,
        formState: {
            errors,
        },
        handleSubmit
    } = useForm({})

    const sendEmail = (formData) => {
        emailjs
            .send(service_id, template_id, formData, key)
            .then(
                (result) => {
                    console.log(formData)
                    console.log(result.text);
                },
                (error) => {
                    console.log(error.text);
                }
            );
        reset();
    };




    return (
            <Form  onSubmit={handleSubmit(sendEmail)}>
                <Form.Group className="d-flex flex-column mb-3">
                    <Form.Label className="header_text text-start">Your Name</Form.Label>
                    <Form.Control type="text" name="user_name" placeholder="Enter name"
                                  {...register('user_name', {
                                      required: "Поле обязательно к заполнению",
                                  })}
                    />
                    {errors?.userName && <p className='text-danger'>{errors?.userName?.message || "Error"}</p>}
                </Form.Group>
                <Form.Group className="d-flex flex-column mb-3">
                    <Form.Label className="header_text text-start">Your Email</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" name="user_email"
                                  {...register('user_email', {
                                      required: "Поле обязательно к заполнению",
                                      pattern: {
                                          value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                                          message: "Пожалуйста, введите действительный адрес электронной почты!"
                                      }
                                  })}

                    />
                    {errors?.email && <p className='text-danger'>{errors?.email?.message || "Error"}</p>}

                </Form.Group>
                <Form.Group className="d-flex flex-column mb-3">
                    <Form.Label className="header_text text-start">Your Message</Form.Label>
                    <textarea name="message" placeholder="Enter message"
                              {...register('message')   }
                    ></textarea>
                </Form.Group>
                <button className="btn btn-success"  type='submit'>Send Message <AiOutlineSend className="mx-2"/></button>
            </Form>
    )
}

export default ContactForm;