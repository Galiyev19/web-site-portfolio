import React from 'react'
import {Link} from 'react-router-dom'

import { Container } from 'react-bootstrap';


import "./Footer.css"

const Footer = () => {

    return (
        <Container bg="dark" fluid={true} className="footer">
            <Container fluid="xl">
                <div className='footer_item'>
                    <h3 className='footer_logo_text'>Galiyev Alisher</h3>
                    <div className='footer_menu_block'>
                        <Link to="/" className='footer_link_text'>Home</Link>
                        <Link to="/projects" className='footer_link_text'>Projects</Link>
                        <Link to="/contacts" className='footer_link_text'>Contact me</Link>
                    </div>
                </div>
            </Container>
        </Container>
    )

}

export default Footer;