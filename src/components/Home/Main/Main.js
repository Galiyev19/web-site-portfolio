import React from 'react'


import {FiGitlab} from 'react-icons/fi'
import {AiOutlineGithub} from 'react-icons/ai'
import { motion } from "framer-motion";

import CV from '../../../assets/pdf-cv/CV.pdf';


import "./Main.css"

const Main = () => {

    const textAnimation = {
        hidden: {
            x: -100,
            opacity: 0,
        },
        visible: custom => ({
            x: 0,
            opacity: 1,
            transition: {delay : custom * 0.4},
            type: "spring"
        }),
    }


  

    return (
        <>
            <motion.div 
            initial="hidden"
            whileInView="visible"
            className='main_block_head'>
                <motion.h1 custom={1} variants={textAnimation} className='header_title'>
                    Hi, my name is <em>Alisher</em> 
                    <br/>
                    a frontend developer
                </motion.h1>
                <motion.p custom={2} variants={textAnimation} className='header_text'>with passion for learning and creating.</motion.p>
            </motion.div>
            <motion.div   initial="hidden" whileInView="visible" className="d-flex w-100 align-items-center justify-content-center mt-2 mb-5">
                    <motion.a custom={2} href={CV} download="cv(rus) version" className="link_download">Download CV</motion.a>
            </motion.div>
            <motion.div  initial="hidden" whileInView="visible" className='main_link_block'>
                <motion.h4 className='main_head_h2' custom={2} variants={textAnimation}>My GitLab and GitHub </motion.h4>
                <motion.div  custom={2} variants={textAnimation} className='main_link_block_item'>
                    <motion.a  whileHover={{scale: 1.1}} href="https://gitlab.com/Galiyev19" className='link_block'>
                        <h4>GitLab</h4>
                        <FiGitlab className='text-warning fs-1'/>
                    </motion.a>
                    <motion.a  whileHover={{scale: 1.1}} href="https://github.com/Galiyev19" className='link_block'>
                        <h4>GitHub</h4>
                        <AiOutlineGithub className='text-warning fs-1'/>
                    </motion.a>
                </motion.div>
            </motion.div>
        </>
    )
}

export default Main;