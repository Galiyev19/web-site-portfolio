import React from 'react'
import {motion} from 'framer-motion'


import html from '../../../assets/images/html.webp'
import css from '../../../assets/images/css.png'
import react from '../../../assets/images/React.png'
import vue from '../../../assets/images/Vue.png'
import star from '../../../assets/images/star.png'   



import "./Skils.css"

const Skils = () => {


    const textAnimation = {
        hidden: {
            x: -100,
            opacity: 0,
        },
        visible: custom => ({
            x: 0,
            opacity: 1,
            transition: {delay : custom * 0.4},
            type: "spring"
        }),
    }


    return (
        <motion.div className='d-flex flex-column text-center w-100'  initial="hidden" whileInView="visible">
            <motion.h2 custom={3} variants={textAnimation} className='mb-4 header_title'>SKILLS</motion.h2>
            <motion.div custom={4} variants={textAnimation} className='d-flex flex-wrap align-items-center justify-content-center w-100'>
                <div className='d-flex  align-items-center flex-column mx-2 mb-2'>
                    <h2 className='mb-2 header_skils_text'>HTML</h2>
                    <img src={html} className="skils_img mb-2" alt="html"/>
                    <div className="d-flex align-items-center justify-content-center w-100 mt-2">
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                    </div>
                </div>
                <div className='d-flex align-items-center flex-column mx-2 mb-2 '>
                    <h2 className='mb-2 header_skils_text'>CSS</h2>
                    <img src={css} className="skils_img mb-2 " alt="css"/>
                    <div className="d-flex align-items-center justify-content-center w-100 mt-2">
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                    </div>
                </div>
                <div className='d-flex align-items-center flex-column mx-2  mb-2'>
                    <h2 className='mb-2 header_skils_text'>React JS</h2>
                    <img src={react} className="skils_img mb-2" alt="react"/>
                    <div className="d-flex align-items-center justify-content-center w-100 mt-2">
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                    </div>
                </div>
                <div className='d-flex align-items-center flex-column mx-2  mb-2'>
                    <h2 className='mb-2 header_skils_text'>React Native</h2>
                    <img src={react} className="skils_img mb-2" alt="react"/>
                    <div className="d-flex align-items-center justify-content-center w-100 mt-2">
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                    </div>
                </div>
                <div className='d-flex align-items-center flex-column mx-2  mb-2'>
                    <h2 className='mb-2 header_skils_text'>Vue JS</h2>
                    <img src={vue} className="skils_img mb-2" alt="react"/>
                    <div className="d-flex align-items-center justify-content-center w-100 mt-2">
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                        <img src={star} className="mx-1" alt="star"/>
                    </div>
                </div>
            </motion.div>
        </motion.div>
    )

}

export default Skils;