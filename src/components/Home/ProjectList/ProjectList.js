import React from 'react'
import { Link } from 'react-router-dom'
import {motion} from 'framer-motion'

import projectsData from "../../../helpers/projectListData";
import "./ProjectList.css"

const ProjectList = () => {


    const listAnimation = {
        hidden: {
            x: -100,
            opacity: 0,
        },
        visible: custom => ({
            x: 0,
            opacity: 1,
            transition: {delay : custom * 0.4},
            type: "spring"
        }),
    }

    return (
        <motion.div
        initial="hidden"
        whileInView="visible"
        className=' flex-column w-100 align-items-center justify-content-center text-center'>
            <motion.div variants={listAnimation} custom={4} className="projectListGrid">
                {
                    projectsData.map(item =>
                        <Link   to={`/projects/${item.id}`} key={item.id} className="project_item">
                            <img src={item.mainImg} className='w-100 h-75 rounded'/>
                            <div className="nameProject">
                                <h4>{item.title}</h4>
                            </div>
                        </Link>)
                }
            </motion.div>
        </motion.div>
    )
}

export default ProjectList;