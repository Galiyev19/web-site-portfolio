import React from 'react'

import Main from './Main/Main';
import ProjectList from './ProjectList/ProjectList';
import Skils from './Skils/Skils';


import { motion, AnimatePresence } from 'framer-motion'

// IMPORT BOOTSTRAP COMPONENTS
import { Container } from 'react-bootstrap';

import './Home.css'

const Home = () => {


    return (
        <AnimatePresence>
            <motion.div
                initial={{ y: "100%" }}
                animate={{ y: "0%" }}
                exit={{ opacity: 0 }}
                transition={{ duration: 0.75, ease: "easeOut" }}
                className='home_container'>
                <div className='main'>
                    <Main />
                </div>
                <div className='projects'>
                    <div className='main'>
                        <ProjectList />
                    </div>
                </div>
                <div className='main'>
                    <Skils />
                </div>
            </motion.div>
        </AnimatePresence>
    )
}

export default Home;