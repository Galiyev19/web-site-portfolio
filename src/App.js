import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';

// IMPORT MY COMPONENTS
import Home from './components/Home/Home';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Projects from './components/Projects/Projects';
import ProjectDetail from "./components/ProjectDetail/ProjectDetail";
import ContactMe from './components/ContactMe/ContactMe';

import './App.css';



const App = () => {



  return (
      <div className="App">
          <BrowserRouter>
              <Header/>
              <Routes>
                  <Route path='/' element={<Home/>}/>
                  <Route path='/projects' element={<Projects/>}/>
                  <Route path='/projects/:id' element={<ProjectDetail/>}/>
                  <Route path='/contacts'  element={<ContactMe/>}/>
              </Routes>
              <Footer/>
          </BrowserRouter>
      </div>

  );
}

export default App;
